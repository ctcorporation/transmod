﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TransMod_Manager.Interfaces;
using TransMod_Manager.Models;

namespace TransMod_Manager.Repositories
{
    public class EnumTypesRepository : GenericRepository<EnumTypes>, IEnumTypesRepository
    {
        public EnumTypesRepository(TransModContext context)
            : base(context)
        {

        }

        public TransModContext TransModContext
        {
            get
            {
                return TContext as TransModContext;
            }
        }
    }
}