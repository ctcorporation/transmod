﻿using System;
using System.Linq.Expressions;
using LinqKit;
using TransMod_Manager.Classes;
using TransMod_Manager.Interfaces;
using TransMod_Manager.Models;

namespace TransMod_Manager.Repositories
{
    public class JobHeaderRepository : GenericRepository<JobHeader>, IJobHeaderRepository
    {
        public JobHeaderRepository(TransModContext context)
            : base(context)
        {

        }


       
        public TransModContext TransModContext
        {
            get
            {
                return TContext as TransModContext;
            }
        }
    }
}