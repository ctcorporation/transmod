﻿using TransMod_Manager.Interfaces;
using TransMod_Manager.Models;
using Address = TransMod_Manager.Models.Address;

namespace TransMod_Manager.Repositories
{
    public class AddressRepository : GenericRepository<Address>, IAddressRepository
    {
        public AddressRepository(TransModContext context)
            : base(context)
        {


        }

        public TransModContext TransModContext
        {
            get
            {
                return TContext as TransModContext;
            }
        }

    }
}