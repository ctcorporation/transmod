﻿using TransMod_Manager.Interfaces;
using TransMod_Manager.Models;

namespace TransMod_Manager.Repositories
{
    public class ImageRepository : GenericRepository<Image>, IImageRepository
    {
        public ImageRepository(TransModContext context)
            : base(context)
        {

        }

        public TransModContext TransModContext
        {
            get
            {
                return TContext as TransModContext;
            }
        }
    }
}