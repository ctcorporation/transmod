﻿using TransMod_Manager.Interfaces;
using TransMod_Manager.Models;

namespace TransMod_Manager.Repositories
{
    public class TransportLegRepository : GenericRepository<TransportLeg>, ITransportLegRepository

    {
        public TransportLegRepository(TransModContext context)
            : base(context)
        {

        }

        public TransModContext TransModContext
        {
            get
            { return TContext as TransModContext; }
        }
    }
}