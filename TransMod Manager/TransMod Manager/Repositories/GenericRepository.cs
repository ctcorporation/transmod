﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;
using TransMod_Manager.Interfaces;
using TransMod_Manager.Models;

namespace TransMod_Manager.Repositories
{


    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        #region members
        protected readonly TransModContext TContext;
        #endregion

        #region properties
        #endregion

        #region constructors
        public GenericRepository(TransModContext context)

        {
            TContext = context;

        }
        #endregion

        #region methods
        public void Add(TEntity entity)
        {
            TContext.Set<TEntity>().Add(entity);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            TContext.Set<TEntity>().AddRange(entities);
        }

        public void Remove(TEntity entity)
        {
            TContext.Set<TEntity>().Remove(entity);
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            TContext.Set<TEntity>().RemoveRange(entities);
        }

        public TEntity Get(Guid id)
        {
            return TContext.Set<TEntity>().Find(id);
        }

        public IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return TContext.Set<TEntity>().Where(predicate);
        }

        public IQueryable<TEntity> Find(string filter, object[] paramList)
        {
            return TContext.Set<TEntity>().Where(filter, paramList);

        }
        public IQueryable<TEntity> GetAll()
        {
            return TContext.Set<TEntity>();
        }


        #endregion

    }
}