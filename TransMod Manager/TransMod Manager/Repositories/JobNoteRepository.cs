﻿using TransMod_Manager.Interfaces;
using TransMod_Manager.Models;

namespace TransMod_Manager.Repositories
{
    public class JobNoteRepository : GenericRepository<JobNote>, IJobNoteRepository
    {
        public JobNoteRepository(TransModContext context)
            : base(context)
        {

        }

        public TransModContext TransModContext
        {
            get
            {
                return TContext as TransModContext;
            }
        }

    }
}