﻿using System.Collections.Generic;
using TransMod_Manager.Models;

namespace TransMod_Manager.ViewModels
{
    public class JobHeaderViewModel
    {
        public IEnumerable<JobHeader> Jobs { get; set; }
        public Organisation Organisation { get; set; }

        public Enums Enums { get; set; }


    }
}