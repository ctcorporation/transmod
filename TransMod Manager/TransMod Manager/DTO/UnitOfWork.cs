﻿using System;
using System.Data.Entity.Validation;
using TransMod_Manager.Interfaces;
using TransMod_Manager.Models;
using TransMod_Manager.Repositories;

namespace TransMod_Manager.DTO
{
    public class UnitOfWork : IUnitOfWork
    {
        #region members
        private readonly TransModContext _context;
        private string _errMsg;

        private bool disposed = false;
        #endregion

        #region properties
        public string ErrMessage
        {
            get
            {
                return _errMsg;
            }
            set
            {
                _errMsg = value;
            }
        }
        // All of Repository Properties go here. 

        public IAddressRepository Addresses
        {
            get; private set;
        }

        public IEnumRepository TransEnums
        {
            get;
            private set;
        }

        public IEnumTypesRepository TransEnumTypes
        {
            get; private set;
        }

        public IImageRepository Images
        {
            get;
            private set;
        }

        public IJobHeaderRepository JobHeaders
        {
            get;
            private set;
        }

        public IJobNoteRepository JobNotes
        {
            get;
            private set;
        }

        public IOrganisationRepository Organisations
        {
            get;
            private set;
        }

        public IRunLinkRepository RunLinks
        {
            get;
            private set;

        }

        public IRunSheetRepository RunSheets
        {
            get;
            private set;
        }
        public ITransportLegRepository TransportLegs
        {
            get;
            private set;
        }
        #endregion

        #region constructors
        public UnitOfWork(TransModContext context)
        {
            _context = context;
            AddRepos();
        }
        #endregion

        #region methods
        private void AddRepos()
        {
            Addresses = new AddressRepository(_context);
            TransEnums = new EnumRepository(_context);
            TransEnumTypes = new EnumTypesRepository(_context);
            Images = new ImageRepository(_context);
            JobHeaders = new JobHeaderRepository(_context);
            JobNotes = new JobNoteRepository(_context);
            Organisations = new OrganisationRepository(_context);
            RunLinks = new RunLinkRepository(_context);
            RunSheets = new RunSheetRepository(_context);
            TransportLegs = new TransportLegRepository(_context);
        }


        public int Complete()
        {
            var commit = 0;
            try
            {
                commit = _context.SaveChanges();
            }
            catch (InvalidOperationException ex)
            {
                _errMsg += ex.GetType().Name + "Found: Error was: " + ex.Message + ":" + ex.InnerException.Message + Environment.NewLine;
            }
            catch (DbEntityValidationException ex)
            {
                foreach (DbEntityValidationResult validationResult in ex.EntityValidationErrors)
                {
                    string entityName = validationResult.Entry.Entity.GetType().Name;
                    foreach (DbValidationError error in validationResult.ValidationErrors)
                    {
                        _errMsg += entityName + "." + error.PropertyName + ": " + error.ErrorMessage + Environment.NewLine;

                    }
                }
            }

            return commit;
        }

        #endregion

        #region helpers
        public bool DBExists()
        {
            bool exists = false;
            exists = _context.Database.Exists();
            return exists;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposed = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~UnitOfWork()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {

            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}