﻿using System.Web.Mvc;

namespace TransMod_Manager.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "TransMod maintenance model.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "csr@ctcorp.com.au.";

            return View();
        }
    }
}