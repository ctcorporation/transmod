﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using TransMod_Manager.DTO;
using TransMod_Manager.Interfaces;
using TransMod_Manager.Models;
using TransMod_Manager.ViewModels;

namespace TransMod_Manager.Controllers
{
    public class JobsController : Controller
    {
        // GET: Jobs
        public ActionResult Index(string dateFrompicker, string dateToPicker, string descr, string jobno, string ddlCustomer, string ddlStatus)
        {

            string sfrom = dateFrompicker ?? DateTime.Now.AddDays(-7).ToString();
            string sto = dateToPicker ?? DateTime.Now.ToString();
            DateTime dtFrom;
            DateTime dtTo;
            //Builds Dynamic Predicates 
            //If the JobNo field has data otherwise filter - -;
            var jlist = !string.IsNullOrEmpty(jobno) ? JobNoStarts(jobno) : null;
            //If the Description field is not empty. Add Descrptiion filter.
            if (jlist == null)
            {
                jlist = string.IsNullOrEmpty(descr) ? null : ContainsInDesc(descr);
            }
            else

            {
                jlist = string.IsNullOrEmpty(descr) ? jlist : jlist.And(ContainsInDesc(descr));
            }

            //Add Status filter
            if (jlist == null)
            {
                jlist = string.IsNullOrEmpty(ddlStatus) ? null : IsJobStatus(ddlStatus);
            }
            else
            {
                jlist = string.IsNullOrEmpty(ddlStatus) ? jlist : jlist.And(IsJobStatus(ddlStatus));
            }
            //Add customer Filter. 
            if (jlist == null)
            {
                jlist = string.IsNullOrEmpty(ddlCustomer) ? null : CustFilter(ddlCustomer);
            }
            else
            {
                jlist = string.IsNullOrEmpty(ddlCustomer) ? jlist : jlist.And(CustFilter(ddlCustomer));
            }

            if (DateTime.TryParse(sfrom, out dtFrom))
            {

            }
            if (DateTime.TryParse(sto, out dtTo))
            {

            }
            //Set a Date Filter;
            jlist = jlist == null ? IsCurrent(dtFrom, dtTo) : jlist.And(IsCurrent(dtFrom, dtTo));
            JobHeaderViewModel jobs = new JobHeaderViewModel();
            TransModContext context = new TransModContext();
            ViewBag.JobNo = jobno;
            ViewBag.Descr = descr;
            ViewBag.DFrom = dtFrom;
            ViewBag.DTo = dtTo;
            ViewBag.CustomerList = GetCustList();
            ViewBag.SelCust = ddlCustomer;
            ViewBag.SelStatus = ddlStatus;
            ViewBag.StatusList = GetStatList();
            var dateFrom = DateTime.Today.AddDays(-7);
            var dateTo = DateTime.Now;
            if (jlist != null)
            {
                //Create a list of the jobs based upon the Filter. 

                jobs.Jobs = context.JobHeaders.Where(jlist).AsQueryable()
                                                .Include(o => o.Organisation)
                                                .Include(e => e.Enum)
                                                .OrderBy(j => j.J_TransportJobNo).ThenByDescending(j => j.J_CreateDate).ToList();

            }

            context.Dispose();
            return View(jobs);
        }



        private SelectList GetStatList()
        {
            //Create a List<Enum>
            // SQl Query = Select E_ID, E_ENUMTYPE from ENUM Inner Join ENUMTYPE on E_ENUMTYPE = Z_ID where Z_Name="Job Status" Order by E_NAME
            List<Enums> statList = new List<Enums>();
            using (IUnitOfWork uow = new UnitOfWork(new TransModContext()))
            {
                statList = uow.TransEnums.GetAll()
                                     .Include(T => T.EnumType)
                                     .Where(T => T.EnumType.Z_Name == "Job Status")
                                     .OrderBy(E => E.E_Name).ToList();
            }
            //ViewBag SelectList to fill the DropDownList for the search Section.
            return new SelectList(statList, "E_Id", "E_Name");
        }

        private SelectList GetCustList()
        {
            // Create a List<Organisation> 
            // SQL Query = Select * from Organisation where O_ISCONSIGNEE=False AND O_ISACTIVE=True Order by O_CODE
            List<Organisation> customerList = new List<Organisation>();
            using (IUnitOfWork uow = new UnitOfWork(new TransModContext()))
            {
                customerList = uow.Organisations.Find(c => c.O_IsConsignee == false && c.O_Isactive == true).OrderBy(c => c.O_Code).ToList();
            }
            //ViewBag SelectList to fill the DropDownLists on the Index Page Search Section            
            return new SelectList(customerList, "O_ID", "O_Code", null);
        }

        private Expression<Func<JobHeader, bool>> CustFilter(string ddlCustomer)
        {
            var predicate = PredicateBuilder.False<JobHeader>();
            Guid v;
            if (Guid.TryParse(ddlCustomer, out v))
            {
                predicate = predicate.Or(p => p.J_O_Customer == v);

                return predicate;
            }
            return null;
        }

        private Expression<Func<JobHeader, bool>> IsJobStatus(string ddlStatus)
        {
            var predicate = PredicateBuilder.False<JobHeader>();
            int v = 0;
            if (int.TryParse(ddlStatus, out v))
            {
                predicate = predicate.Or(p => p.J_Status == v);

                return predicate;
            }
            return null;

        }

        private Expression<Func<JobHeader, bool>> ContainsInDesc(params string[] keywords)
        {
            var predicate = PredicateBuilder.False<JobHeader>();
            foreach (var keyword in keywords)
            {
                string temp = keyword;
                predicate = predicate.Or(p => p.J_Description.Contains(temp));
            }
            return predicate;
        }
        private Expression<Func<JobHeader, bool>> JobNoStarts(params string[] keywords)
        {
            var predicate = PredicateBuilder.False<JobHeader>();
            foreach (var keyword in keywords)
            {
                string temp = keyword;
                predicate = predicate.Or(p => p.J_TransportJobNo.StartsWith(temp));
            }
            return predicate;
        }

        private Expression<Func<JobHeader, bool>> IsCurrent(DateTime dtFrom, DateTime dtTo)
        {
            var predicate = PredicateBuilder.True<JobHeader>();
            return jh => (jh.J_CreateDate >= (DateTime)dtFrom) &&
                         (jh.J_CreateDate <= (DateTime)dtTo);

        }
    }
}