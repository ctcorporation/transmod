﻿using TransMod_Manager.Models;

namespace TransMod_Manager.Interfaces
{
    public interface IAddressRepository : IGenericRepository<Address>
    {
        TransModContext TransModContext { get; }
    }
}