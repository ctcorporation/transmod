﻿using TransMod_Manager.Models;

namespace TransMod_Manager.Interfaces
{
    public interface IEnumTypesRepository : IGenericRepository<EnumTypes>
    {
        TransModContext TransModContext { get; }
    }
}