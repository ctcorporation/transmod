﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TransMod_Manager.Models;

namespace TransMod_Manager.Interfaces
{
    public interface IRunSheetRepository : IGenericRepository<RunSheet>
    {
        TransModContext TransModContext { get; }
    }
}