﻿using TransMod_Manager.Models;

namespace TransMod_Manager.Interfaces
{
    public interface IJobHeaderRepository : IGenericRepository<JobHeader>
    {
        TransModContext TransModContext { get; }
    }
}