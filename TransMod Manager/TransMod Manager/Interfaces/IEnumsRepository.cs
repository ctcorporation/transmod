﻿using TransMod_Manager.Models;

namespace TransMod_Manager.Interfaces
{
    public interface IEnumRepository : IGenericRepository<Enums>
    {
        TransModContext TransModContext { get; }
    }
}