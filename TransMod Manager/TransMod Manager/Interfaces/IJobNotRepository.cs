﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TransMod_Manager.Models;

namespace TransMod_Manager.Interfaces
{
    public interface IJobNoteRepository : IGenericRepository<JobNote>
    {
        TransModContext TransModContext { get; }
    }
}