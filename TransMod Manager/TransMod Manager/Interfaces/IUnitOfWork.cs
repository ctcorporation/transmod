﻿

using System;

namespace TransMod_Manager.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IAddressRepository Addresses { get; }
        string ErrMessage { get; set; }
        IImageRepository Images { get; }
        IJobHeaderRepository JobHeaders { get; }
        IJobNoteRepository JobNotes { get; }
        IOrganisationRepository Organisations { get; }
        IRunLinkRepository RunLinks { get; }
        IRunSheetRepository RunSheets { get; }
        IEnumRepository TransEnums { get; }
        IEnumTypesRepository TransEnumTypes { get; }
        ITransportLegRepository TransportLegs { get; }

        int Complete();
        bool DBExists();
    }
}