﻿using TransMod_Manager.Models;

namespace TransMod_Manager.Interfaces
{

    public interface IOrganisationRepository : IGenericRepository<Organisation>
    {
        TransModContext TransModContext { get; }
    }
}