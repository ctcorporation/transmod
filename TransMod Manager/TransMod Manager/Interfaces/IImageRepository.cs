﻿using TransMod_Manager.Models;

namespace TransMod_Manager.Interfaces
{
    public interface IImageRepository : IGenericRepository<Image>
    {
        TransModContext TransModContext { get; }
    }
}