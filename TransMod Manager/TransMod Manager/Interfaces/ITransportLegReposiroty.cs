﻿using TransMod_Manager.Models;

namespace TransMod_Manager.Interfaces
{
    public interface ITransportLegRepository : IGenericRepository<TransportLeg>
    {
        TransModContext TransModContext { get; }
    }
}