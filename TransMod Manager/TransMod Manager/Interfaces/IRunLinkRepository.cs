﻿using TransMod_Manager.Models;

namespace TransMod_Manager.Interfaces
{
    public interface IRunLinkRepository : IGenericRepository<RunLink>
    {
        TransModContext TransModContext { get; }
    }
}